/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stateless;

import entidad.Libro;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Armand
 */
@Stateless
public class LibroBean implements LibroBeanRemote 
{
    
    @PersistenceContext(unitName="Libreria-ejbPU")
    EntityManager em;
    
    protected Libro libro;
    protected Collection<Libro> listaLibros;
    //ya tengo mis atributos de clase
    
    
    
    @Override
    public void add(String titulo, String autor, BigDecimal precio) 
    {
        if(libro==null)
        {
            libro= new Libro( titulo,  autor,  precio);
            
            em.persist(libro);
            libro=null;
        }
    }

    @Override
    public Collection<Libro> getAllLibros() 
    {
        listaLibros= em.createNamedQuery("Libro.findAll").getResultList();//el resultado lo pongo en una lista
        return listaLibros;
       }
   // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    @Override
    public Libro buscaLibro(int id) {
        
        libro=em.find(Libro.class, id);
        return libro;
    }

    @Override
    public void actualizaLibro(Libro libro, String titulo, String autor, BigDecimal precio) {
        
        if(libro!=null)
        {
            libro.setTitulo(titulo);
            libro.setAutor(autor);
            libro.setPrecio(precio);
            em.merge(libro);
        }
       }

    @Override
    public void elimina(int id) 
    {
        libro=buscaLibro(id);
        em.remove(libro); 
     
    }

   
}
