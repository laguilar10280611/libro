/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stateless;

import entidad.Libro;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Remote;

/**
 *
 * @author Armand
 */
@Remote
public interface LibroBeanRemote 
{
    public void add(String Titulo, String autor, BigDecimal precio);
    public Collection<Libro> getAllLibros();
    
    public Libro buscaLibro(int id);
    public void actualizaLibro(Libro libro, String titulo, String autor, BigDecimal precio);
    public void elimina(int id);
    
    
    
}
