<%-- 
    Document   : forma
    Created on : 29/10/2014, 08:05:32 AM
    Author     : Armand
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>NUEVO LIBRO </title>
        <style>
            body
            {
                background: #DFDDDD;
                width: 100%;
                
            }
            td
            {
                text-align: center;
                color:#FE9D16;
                font-size: 20px;
                width: 37%;
            }
            h1
            {
                text-align: center;
                color:orangered;
            }
            input
            {
                background: orangered;
                padding: 10px;
            }
        </style>
    </head>
    <body >
        <h1>Libreria</h1><hr>
        <form name="formulario" action="ClienteWeb.jsp" method="POST">
            <table>
                <tr>
                    <td>Introduzca el Titulo:</td>
                    <td><input type="text" name="t1" size="20"></td>
                </tr>
                
                <tr>
                    <td>Introduzca el Nombre del Autor:</td>
                    <td><input type="text" name="aut" size="20"></td>
                </tr>
                <tr>
                    <td>Introduzca el precio:</td>
                    <td><input type="text" name="precio" size="20"/></td>
                </tr>
                
                <tr>
                    <td> <input type="submit" name="crear" value="Crear" /></td>
                    <td><input type="reset" name="limpiar" value="Limpiar" /></td>
                </tr>
                
            </table>
        </form>
        
    </body>
</html>
