<%@page import="entidad.*, stateless.*, java.math.BigDecimal, javax.naming.*, java.util.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ELIMINAR </title>
        <style>
            body
            {
                background: #DFDDDD;
                width: 100%;
                
            }
            
            h1,p
            {
                text-align: center;
                color:orangered;
            }
            b{color:black;}
            a
            {
                background: orangered;
                padding: 10px;
            }
            
            
        </style>
    </head>
</html>
<%! private LibroBeanRemote librocat=null;

String s0;
Collection list;
Libro libro;
InitialContext context;
public void jsplinit()
{
    try
    {
        context= new InitialContext();
        librocat=(LibroBeanRemote) context.lookup(LibroBeanRemote.class.getName());
        
        System.out.println("Cargando el Catalogo Bean"+ librocat);
    } 
    
    catch(Exception ex)
    {
        System.out.println("ERROR: "+ex.getMessage());
    }
}
public void jspDestroy()
{
    librocat=null;
}
%>

<%
    try
    {
        context= new InitialContext();
        librocat=(LibroBeanRemote) context.lookup(LibroBeanRemote.class.getName());
        s0=request.getParameter("id");
        
        if(s0!=null)
        {
            Integer id = new Integer(s0);
            
            libro=librocat.buscaLibro(id.intValue());
            if(libro==null)
            {
                %>
                <h1>EL LIBRO NO EXISTE EN LA BASE DE DATOS!!</h1>
                <a href="index.html"> MENU PRINCIPAL</a><br>
<%
            }
            else
            {
                librocat.elimina(id.intValue());  
                %>
                <jsp:forward page="muestraDatos.jsp"> 
                <jsp:param name="id" value= "<%=s0%>"/>
                
                </jsp:forward>
<%
            }
            System.out.println(" REGISTRO ENCONTRADO " );
        }
            %>
            
       <%     
    }//fin del tray
    catch(Exception e)
    {
        e.printStackTrace();
    }
    %>