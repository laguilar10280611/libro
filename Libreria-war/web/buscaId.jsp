<%-- 
    Document   : buscaId
    Created on : 5/11/2014, 07:50:41 AM
    Author     : Armand
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUSCA ID </title>
        <style>
            body
            {
                background: #DFDDDD;
                width: 100%;
                
            }
            td
            {
                text-align: center;
                color:#FE9D16;
                font-size: 20px;
                width: 37%;
            }
            h1
            {
                text-align: center;
                color:orangered;
            }
            input
            {
                background: orangered;
                padding: 10px;
            }
        </style>
        <script>
            function validar(id)
            {
                if(id=="")
                {
                    alert("INGRESA UN ID DE LIBRO");
                }
                else
                {
                    if (!/^([0-9])*$/.test(id))
                    {
                        alert("EL ID  " +id+ "  NO ES VÁLIDO");
                    }

                }

            }
        </script>
    </head>
    <body >
        <h1>Libreria</h1><hr>
        <form name="formulario" action="ClienteWeb1.jsp" method="POST">
            <table>
                <tr>
                    <td>Introduzca el Id del Libro</td>
                    <td><input type="text" name="id" size="20" onchange="validar(this.value);"></td>
                </tr>
                
                <tr>
                    <td> <input type="submit" name="buscar" value="Buscar" /></td>
                    <td><input type="reset" name="limpiar" value="Limpiar" /></td>
                </tr>
                
            </table>
        </form>
        
    </body>
</html>
